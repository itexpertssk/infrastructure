## Graylog centralized logging
The Graylog server along with required dependencies (MongoDB, Elasticsearch) can be started
using the docker compose. Just run `docker compose up -d` from the docker/graylog folder.
* The Graylog UI is accessible on [http://192.168.122.230:9000/](http://192.168.122.230:9000/) (admin/admin)
##  Prometheus monitoring
The Prometheus server along with  Grafana  can be started using the docker compose. 
Just run `docker compose up -d` from the docker/prometheus folder.
* The Prometheus UI is accessible on [http://192.168.122.230:19090/](http://192.168.122.230:19090/)
* The Grafana UI is accessible on [http://192.168.122.230:3000/](http://192.168.122.230:3000/)
## Consul Discovery
Consul is used as service registry.
* clone Consul example repo `git clone https://github.com/hashicorp/learn-consul-docker.git` into consul directory
* Run `docker compose up -d` from the consul/learn-consul-docker/datacenter-deploy-secure
* The Consul UI is accessible on [http://192.168.122.230:8500/](http://192.168.122.230:8500/)



